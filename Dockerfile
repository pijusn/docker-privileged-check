FROM busybox

MAINTAINER Pijus Navickas <pijus.navickas@gmail.com>

COPY check.sh /opt/check.sh
RUN chmod +x /opt/check.sh

ENTRYPOINT ["/opt/check.sh"]
