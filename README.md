# Docker Image: priviliged-check

This image is indended to check whether container is running in `--priviliged` mode.

When run, it will return 0 if it's running in privileged mode or error code otherwise.